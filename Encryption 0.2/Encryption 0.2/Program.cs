﻿using System;
using System.Text;

namespace Encryption_0._2
{
    class Program
    {
        static void Main(string[] args)
        {
            string text = "Text to be encrypted";
            UnicodeEncoding Converter = new UnicodeEncoding();
            byte[] data = Converter.GetBytes(text);
            Encrypt encryption = new Encrypt();
            byte[] encrypted = encryption.Encryption(data);
            string decrypted = Converter.GetString(encryption.Decryption(encrypted));
            Console.WriteLine(text);
            Console.WriteLine(decrypted);
            Console.ReadLine();
        }
    }
}
