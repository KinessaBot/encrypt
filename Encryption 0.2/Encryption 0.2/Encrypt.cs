﻿using System.Security.Cryptography;


namespace Encryption_0._2
{
    class Encrypt
    {
        private byte[] encrypted;
        private string publicKey;
        private string privateKey;

        public Encrypt()
        {
            using (var rsa = new RSACryptoServiceProvider())
            {
                publicKey = rsa.ToXmlString(false);
                privateKey = rsa.ToXmlString(true);
            }
        }

        public byte[] Encryption(byte[] data)
        {
            using (var RSAEncrypt = new RSACryptoServiceProvider())
            {
                RSAEncrypt.FromXmlString(publicKey);
                encrypted = RSAEncrypt.Encrypt(data, false);
            }
            return encrypted;
        }

        public byte[] Decryption(byte[] data)
        {
            byte[] decrypted;
            using (var RSADecrypt = new RSACryptoServiceProvider())
            {
                RSADecrypt.FromXmlString(privateKey);
                decrypted = RSADecrypt.Decrypt(encrypted, false);
            }
            return decrypted;
        }
    }
}
